﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using View;
using Model;
using UniRx;
using UniRx.Triggers;

namespace Presenter
{
    public class HPBarPresenter : MonoBehaviour
    {
        /// <summary>
        /// HPバービュー
        /// </summary>
        private HPBarView _view;

        /// <summary>
        /// ステータスモデル
        /// </summary>
        private StatusModel _model;

        public void Init(HPBarView view, StatusModel model)
        {
            if (view == null)
            {
                Debug.LogError("[HPBarPresenter::Init] view is null");
                return;
            }
            if (model == null)
            {
                Debug.LogError("[HPBarPresenter::Init] model is null");
                return;
            }
            if (model.HP == null)
            {
                Debug.LogError("[HPBarPresenter::Init] model.HP is null");
                return;
            }

            _view = view;
            _model = model;

            _view.Slider.maxValue = model.MaxHP.Value;

            // HPバーの減算
            _model.HP.ObserveEveryValueChanged(x => x.Value)
                .Subscribe(x => _view.Slider.value = x).AddTo(gameObject);

            this.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    transform.rotation = Camera.main.transform.rotation;
                }).AddTo(gameObject);

        }
    }
}
