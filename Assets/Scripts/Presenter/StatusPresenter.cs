﻿using UnityEngine;
using UniRx;

using Model;
using View;

namespace Presenter
{
    public class StatusPresenter : MonoBehaviour
    {
        /// <summary>
        /// ステータスビュー
        /// </summary>
        private StatusView _view;

        /// <summary>
        /// ステータスモデル
        /// </summary>
        private StatusModel _model;

        public void Init(StatusView view, StatusModel model)
        {
            if (view == null)
            {
                Debug.LogError("[StatusPresenter::Init] view is null");
                return;
            }
            if (model == null)
            {
                Debug.LogError("[StatusPresenter::Init] model is null");
                return;
            }

            _view = view;
            _model = model;

            if (_model.Level == null)
            {
                Debug.LogError("[StatusPresenter::Init] model.Level is null");
                return;
            }
            _model.Level.SubscribeToText(_view.Level);

            if (_model.HP == null)
            {
                Debug.LogError("[StatusPresenter::Init] model.HP is null");
                return;
            }
            _model.HP.ObserveEveryValueChanged(x => x.Value)
                .Subscribe(_ => _view.HP.text = GetHPString());

            if (_model.MaxHP == null)
            {
                Debug.LogError("[StatusPresenter::Init] model.MaxHP is null");
                return;
            }
            _model.MaxHP.ObserveEveryValueChanged(x => x.Value)
                .Subscribe(_ => _view.HP.text = GetHPString());

            if (_model.NextExp == null)
            {
                Debug.LogError("[StatusPresenter::Init] model.NextExp is null");
                return;
            }
            _model.NextExp.SubscribeToText(_view.NextExp);
        }

        private string GetHPString()
        {
            string ret = "";
            for (int i = 0; i < _model.HP.Value; ++i)
            {
                ret += "■";
            }
            int calc = _model.MaxHP.Value - _model.HP.Value;
            for (int i = 0; i < calc; ++i)
            {
                ret += "□";
            }
            return ret;
        }
    }
}
