﻿namespace Presenter
{
    public class Presenter<TView, TModel>
    {
        public TView View { get; }
        public TModel Model { get; }

        public Presenter(TView view, TModel model)
        {
            View = view;
            Model = model;
        }
    }
}
