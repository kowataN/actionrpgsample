﻿using UnityEditor;
using UnityEngine;

public class HierarchyMenu
{
    [MenuItem("GameObject/Unit/Enemy", false, 20)]
    [System.Obsolete]
    public static void CreateEnemy()
    {
        string name = "Enemy";
        string prefabPath = "Assets/Resources/Models/Character/RTS Mini Legion Rock Golem/Prefabs/FreeGolem.prefab";

        GameObject gameObject = EditorUtility.CreateGameObjectWithHideFlags(name, HideFlags.HideInHierarchy);

        _ = PrefabUtility.CreatePrefab(prefabPath, gameObject);

        Vector3 position = new Vector3(250, 10, 250);
        gameObject.transform.position = position;

        Editor.DestroyImmediate(gameObject);
    }
}
