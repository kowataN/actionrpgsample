﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class InputManager : SingletonMonoBhv<InputManager>
{
    public int hoge { get; set; } = 100;

    #region 入力制限
    private LockManager _Lock = new LockManager();
    public LockManager Lock { get; private set; }
    #endregion

    #region キー入力関連
    private BoolReactiveProperty _EnabledKey = new BoolReactiveProperty(false);

    /// <summary>
    /// キーボード有効かどうか
    /// </summary>
    public IReadOnlyReactiveProperty<bool> EnabledKey => _EnabledKey;

    private InputKeyborad _InputKey = new InputKeyborad();
    public bool GetKey(KeyCode code)
    {
        return _InputKey.GetKey(code);
    }

    public bool GetKeyDown(KeyCode code)
    {
        return _InputKey.GetKeyDown(code);
    }

    private readonly ReactiveProperty<Vector3> _AxisKey = new ReactiveProperty<Vector3>();
    public IReadOnlyReactiveProperty<Vector3> AxisKey => _AxisKey;
    #endregion

    private void Awake()
    {
        Init();
    }

    /// <summary>
    /// 入力関連を設定します
    /// </summary>
    private void Init()
    {
        //Debug.Log("InputManager Init");
        // キーボードが有効状態で未ロックなら更新を行う
        this.UpdateAsObservable()
            .Where(_ => _EnabledKey.Value && _Lock.IsUnLock())
            .Subscribe(_ => { _InputKey.UpdateInput(); })
            .AddTo(gameObject);

        this.UpdateAsObservable()
            .Where(_ => _EnabledKey.Value && _Lock.IsUnLock())
            .Select(_ => new Vector3(GetAxisKeyHorizontal(), 0, GetAxisKeyVertical()))
            .Subscribe(x => _AxisKey.SetValueAndForceNotify(x))
            .AddTo(gameObject);

        // キーボードが無効になったら初期化する
        _EnabledKey.Where(_ => EnabledKey.Value == false)
                   .Subscribe(_ => { _InputKey.InitInput(); })
                   .AddTo(gameObject);
    }

    /// <summary>
    /// キーボードの有効状態を設定します
    /// </summary>
    /// <param name="flag">If set to <c>true</c> flag.</param>
    public void SetEnabledKeyborad(bool flag)
    {
        _EnabledKey.Value = flag;
    }

    /// <summary>
    /// 水平方向の移動量を返します
    /// </summary>
    /// <returns>The axis key horizontal.</returns>
    public float GetAxisKeyHorizontal()
    {
        return _Lock.IsLock() ? 0.0f : _InputKey.InputHorizontal;
    }

    /// <summary>
    /// 垂直方向の移動量を返します
    /// </summary>
    /// <returns>The axis key vertical.</returns>
    public float GetAxisKeyVertical()
    {
        return _Lock.IsLock() ? 0.0f : _InputKey.InputVertical;
    }

    public bool IsMove()
    {
        if (_Lock.IsLock())
        {
            // ロック中は動かないものとする
            return false;
        }
        return (_InputKey.InputHorizontal != 0 || _InputKey.InputVertical != 0);
    }
}
