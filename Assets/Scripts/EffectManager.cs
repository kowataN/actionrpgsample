﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : SingletonMonoBhv<EffectManager>
{
    /// <summary>
    /// エフェクトのルートパス
    /// </summary>
    private readonly string _prefabPath = "Effect/";

    private Dictionary<string, GameObject> _files = new Dictionary<string, GameObject>();

    public void Load(string name)
    {
        GameObject prefab = Resources.Load(_prefabPath + name) as GameObject;
        if (prefab == null)
        {
            Debug.LogError("[EffectManager::Load] Not Found : " + (_prefabPath + name).ToString());
            return;
        }
        _files.Add(name, prefab);
    }

    public void Unload(string name) => _files.Remove(name);

    public void Clear() => _files.Clear();

    public void Play(string name, GameObject parent)
    {
        GameObject prefab = Instantiate(_files[name]) as GameObject;
        prefab.transform.parent = parent.transform;
        prefab.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        prefab.transform.eulerAngles = new Vector3(-90.0f, 0.0f, 0.0f);

        ParticleSystem ps = prefab.GetComponentInChildren<ParticleSystem>();
        if (ps == null)
        {
            return;
        }
        Destroy(prefab, (float)ps.main.duration);
    }
}
