﻿using UniRx;
using UnityEngine;

namespace Model
{
    public class StatusModel : MonoBehaviour
    {
        /// <summary>
        /// レベル
        /// </summary>
        public IntReactiveProperty Level = new IntReactiveProperty();

        /// <summary>
        /// 最大HP
        /// </summary>
        public IntReactiveProperty MaxHP = new IntReactiveProperty();

        /// <summary>
        /// 現在のHP
        /// </summary>
        public IntReactiveProperty HP = new IntReactiveProperty();

        /// <summary>
        /// 現在の経験値
        /// </summary>
        public IntReactiveProperty Exp = new IntReactiveProperty();

        /// <summary>
        /// 次のレベルまでの必要経験値
        /// </summary>
        public IntReactiveProperty NextExp = new IntReactiveProperty();
    }
}
