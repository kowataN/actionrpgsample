﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UniRx;
using DG.Tweening;

public class GameSelect : MonoBehaviour
{
    [Header("PanelCharacter")]
    public GameObject _PanelMain;

    [Header("SelectCharacter")]
    public GameObject _BtnLeft, _BtnRight;
    public GameObject[] _UnitsPrefabs;
    public Button _BtnOK;
    private Text _Name, _Lv, _Hp, _Mp, _Atk, _Mgc, _Def, _Luc;

    [Header("SelectStageAndLevel")]
    public GameObject _PanelLevel;
    public GameObject _PanelStage;
    public GameObject _BtnBack;
    public GameObject _BtnStart;

    private GameSelectState _State;
    private LockManager _Lock = new LockManager();

    // Use this for initialization
    void Start()
    {
        _State = new GameSelectState(StateInit);
    }

    // Update is called once per frame
    void Update()
    {
        _State.Execute();
    }

    #region state
    private void StateInit()
    {
        Debug.Log("StateInit");
        Fader.Instance.SetColorAndActive(Color.black, true);

        //LockManager.Instance.Lock();

        _State.ChangeState(StateFadeIn);
    }
    private void StateFadeIn()
    {
        Debug.Log("StateFadeIn");
        Fader.Instance.BlackIn(0.6f, StateEndFadeIn);
        _State.ChangeState(null);
    }
    private void StateEndFadeIn()
    {
        Debug.Log("StateEndFadeIn");
        //LockManager.Instance.UnLock();
        _BtnOK.OnClickAsObservable()
              .Subscribe(_ => OnPressedOK()).AddTo(gameObject);

        _State.ChangeState(StateGameSelectIdle);
    }
    private void StateGameSelectIdle()
    {

    }
    private void StateEndCharacterSelect()
    {
        Debug.Log("StateEndCharacterSelect");
        _Lock.Lock();
        _State.ChangeState(null);
    }
    #endregion

    public void OnPressedOK()
    {
        if (_Lock.IsLock()) { return; }
        _Lock.Lock();
        Vector3 target = transform.position;
        target.x -= 1280;
        Sequence seq = DOTween.Sequence()
            .Append(_PanelMain.transform.DOMove(target, 1.0f))
            .OnComplete(() =>
            {
                _State.ChangeState(StateEndCharacterSelect);
            });
    }
}
