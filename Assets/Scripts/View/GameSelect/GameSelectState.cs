﻿using State;

public class GameSelectState : StateBase
{
    public GameSelectState(executeState exec) : base(exec) { }
    public override void Execute()
    {
        if (ExecDelegate != null)
        {
            ExecDelegate();
        }
    }
}
