﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    [SerializeField] private Button _startButton;

    // Use this for initialization
    private void Start()
    {
        InputManager.Instance.SetEnabledKeyborad(true);
        InputManager.Instance.hoge = 1;

        // スタートボタン押下処理
        _startButton.OnClickAsObservable()
            .Subscribe(_ =>
            {
                // TODO: 本当はキャラ選択へ
                SceneManager.LoadScene("Battle", LoadSceneMode.Single);
            })
            .AddTo(gameObject);
    }
}
