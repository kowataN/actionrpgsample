﻿using System;
using UnityEngine;
using UniRx;
using Presenter;
using View;

public class Battle : MonoBehaviour
{
    private readonly string _levelUpEffect = "magic_ring_03";

    private GameObject _player;
    private GameObject _enemyBase;
    private StatusPresenter _statusPresenter;

    public int EnemyCreateInterval = 5;
    public int MaxEnemy = 1;

    [SerializeField] private int _crtEnemyCount;
    [SerializeField] private GameObject _statusViewObj;
    [SerializeField] private Camera _followingCamera;
    [SerializeField] private GameObject _enemyField;

    private int _totalEnemyCount = 0;
    private Player _playerScript;

    // Use this for initialization
    private void Start()
    {
        InputManager.Instance.SetEnabledKeyborad(true);
        EffectManager.Instance.Load(_levelUpEffect);

        CreatePlayer();

        if (_enemyField == null)
        {
            Debug.LogError("[Battle::Start] _enemyField is null");
            return;
        }
        LoadEnemyObject();
        Observable.Timer(TimeSpan.FromSeconds(EnemyCreateInterval),
                         TimeSpan.FromSeconds(EnemyCreateInterval))
                  .Where(_ => _crtEnemyCount < MaxEnemy)
                  .Subscribe(_ => CreateEnemy())
                  .AddTo(gameObject);
    }

    private void CreatePlayer()
    {
        Vector3 position = new Vector3(0, 10, 0);
        string prefabPath = "Models/Character/RTS Mini Legion Footman/Prefabs/Footman_Blue";
        _player = Resources.Load(prefabPath) as GameObject;
        if (_player == null)
        {
            Debug.LogError("[Battle::CreatePlayer] 存在しないPrefabファイル : " + prefabPath);
            return;
        }

        _player = Instantiate(_player, transform);
        _player.transform.position = position;
        _player.name = "Player"; // 「(clone)」が付与されるのでリネーム
        _player.tag = "Player";

        _playerScript = _player.GetComponentInChildren<Player>();
        _playerScript.Init(2); // HP=2
        InitFollowingCamera();

        StatusView view = _statusViewObj.GetComponentInChildren<StatusView>();
        if (view == null)
        {
            Debug.LogError("[Battle::CreatePlayer] StatusView is null");
        }

        _statusPresenter = new StatusPresenter();
        _statusPresenter.Init(view, _playerScript.Status);

        // レベルアップ時エフェクト再生
        _playerScript.Status.Level.DistinctUntilChanged()
            .Where(_ => _playerScript.Status.Level.Value > 1)
            .Subscribe(_ => 
                EffectManager.Instance.Play(_levelUpEffect, _player)
            )
            .AddTo(gameObject);
    }

    private void LoadEnemyObject()
    {
        string prefabPath = "Models/Character/RTS Mini Legion Rock Golem/Prefabs/FreeGolem";
        _enemyBase = Resources.Load(prefabPath) as GameObject;
        if (_enemyBase == null)
        {
            Debug.LogError("[Battle::LoadEnemyObject] 存在しないPrefabファイル : " + prefabPath);
            return;
        }
    }

    private void CreateEnemy()
    {
        Vector3 position = _player.transform.position
                         + new Vector3(UnityEngine.Random.Range(-30, 30), 0,
                             UnityEngine.Random.Range(-30, 30));
        GameObject enemy = Instantiate(_enemyBase, _enemyField.transform);
        enemy.transform.position = position;
        enemy.name = "Enemy" + _totalEnemyCount.ToString(); // 「(clone)」が付与されるのでリネーム
        enemy.tag = "Enemy";

        Enemy script = enemy.GetComponentInChildren<Enemy>();
        script.Init(_player, 20);
        _ = script.Status.HP
            .ObserveEveryValueChanged(x => x.Value)
            .Where(x => x <= 0).First()
            .Subscribe(_ =>
            {
                -- _crtEnemyCount;
                _playerScript.AddExp(10);
            });

        script.SetName(enemy.name.ToString());

        ++ _crtEnemyCount;
        ++ _totalEnemyCount;
    }

    private void InitFollowingCamera()
    {
        if (_followingCamera == null)
        {
            Debug.LogError("[Battle::InitFollowingCamera] _followingCamera is null");
            return;
        }
        CameraController cc = _followingCamera.GetComponentInChildren<CameraController>();
        cc.Init(_player);
    }
}
