﻿using UnityEngine;
using Model;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public abstract class UnitCore : MonoBehaviour
{
    public StatusModel Status;
    protected CharacterController _charaCtrl;
    protected AnimationManager _animationMng;

    protected virtual void Update() { }

    protected virtual void InitCharacterController()
    {
        _charaCtrl = GetComponent<CharacterController>();
    }
    protected void InitAnimator()
    {
        _animationMng = new AnimationManager(GetComponent<Animator>());
        _animationMng.ChangeAnimation(AnimationType.Idle);
    }
}
