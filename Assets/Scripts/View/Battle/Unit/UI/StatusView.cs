﻿using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class StatusView : MonoBehaviour
    {
        [SerializeField] private Text _level;
        public Text Level => _level;

        [SerializeField] private Text _hp;
        public Text HP => _hp;

        [SerializeField] private Text _nextExp;
        public Text NextExp => _nextExp;
    }
}
