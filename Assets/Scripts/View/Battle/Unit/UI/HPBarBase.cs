﻿using UnityEngine;
using UnityEngine.UI;

public abstract class HPBarBase : MonoBehaviour
{
    protected Slider _slider;
    public Slider Slider => _slider;

    // Start is called before the first frame update
    protected void Awake()
    {
        _slider = GetComponent<Slider>();
        if (_slider == null)
        {
            Debug.LogError("[HPBarBase::Awake] HP Slider is null");
        }
    }
}
