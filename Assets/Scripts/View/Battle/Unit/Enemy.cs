﻿using UnityEngine;
using UniRx;
using UnityEngine.AI;

using View;
using Presenter;
using Model;
using System;
using UniRx.Triggers;

public class Enemy : UnitCore
{
    [SerializeField] private GameObject _areaSearchObj;
    [SerializeField] private GameObject _areaAttackObj;
    [SerializeField] private GameObject _hpBarObj;

    private GameObject _target;
    private NavMeshAgent _agent;

    private AreaController _areaSearch;
    private AreaController _areaAttack;

    protected void Awake()
    {
        InitAnimator();
        InitCharacterController();
        _agent = GetComponent<NavMeshAgent>();
    }

    public void Init(GameObject target, int hp)
    {
        _target = target;
        Status.MaxHP = new IntReactiveProperty(hp);
        Status.HP = new IntReactiveProperty(hp);

        // HPバー初期化
        SetupHPBar();

        // 索敵エリアの設定
        SetupSearchArea();

        // 攻撃エリアの設定
        SetupAttackArea();

        // 攻撃を受けた時の処理
        SetupGetHit();

        // 死亡判定
        _animationMng.Dead.ObserveEveryValueChanged(x => x.Value)
            .Where(x => x).First()
            .Subscribe(x =>
            {
                Destroy(gameObject);
            })
            .AddTo(gameObject);

        // 作成直後はNavMeshを止める
        _agent.enabled = false;
    }

    public void SetName(string name)
    {
        HPBarView view = GetComponentInChildren<HPBarView>();
        view.Name.text = name;        
    }
    private void SetupHPBar()
    {
        HPBarView view = GetComponentInChildren<HPBarView>();
        if (view == null)
        {
            //Debug.LogError("[Enemy::SetupHPBar] HPBarView is null");
            return;
        }
        HPBarPresenter presenter = GetComponentInChildren<HPBarPresenter>();
        if (presenter == null)
        {
            //Debug.LogError("[Enemy::SetupHPBar] presenter is null");
            return;
        }

        presenter.Init(view, Status);
    }

    private void SetupSearchArea()
    {
        _areaSearch = _areaSearchObj.GetComponent<AreaController>();
        if (_areaSearch == null)
        {
            Debug.LogError("[Enemy::SetupSearchArea] AreaSearch is null");
            return;
        }
        _areaSearch.InRange
            .ObserveEveryValueChanged(x => x.Value)
            .Where(_ => Status.HP.Value > 0)
            .Subscribe(x =>
            {
                _agent.enabled = x;
                _animationMng.ChangeAnimation(
                    _agent.enabled ? AnimationType.Run : AnimationType.Idle);
            }).AddTo(gameObject);
    }

    private void SetupAttackArea()
    {
        _areaAttack = _areaAttackObj.GetComponent<AreaController>();
        if (_areaAttack == null)
        {
            Debug.LogError("[Enemy::SetupAttackArea] AreaAttack is null");
            return;
        }

        var attackObserve = _areaAttack.InRange
              .DistinctUntilChanged()
              .Where(_ => Status.HP.Value > 0)
              .Publish().RefCount();

        // フラグが更新された時だけNavMeshAgentの状態を更新する
        attackObserve.Subscribe(x => _agent.enabled = !x).AddTo(gameObject);

        // 範囲内なら攻撃させる
        attackObserve.Where(x => x)
            .Subscribe(_ =>
            {
                //Debug.Log("攻撃させる");
                if (_animationMng.IsAnimation(AnimationType.Run) ||
                    _animationMng.IsAnimation(AnimationType.Idle))
                {
                    _animationMng.ChangeAnimation(AnimationType.Attack1);
                }
            }).AddTo(gameObject);

        // 範囲外なら移動
        attackObserve.Where(x => x == false)
            .Subscribe(_ =>
            {
                //Debug.Log("攻撃をやめる");
                _animationMng.ChangeAnimation(AnimationType.Idle);
            }).AddTo(gameObject);
    }

    private void SetupGetHit()
    {
        Player player = _target.GetComponent<Player>();
        if (player == null) { Debug.LogError("[Enemy::SetupGetHit] player is null"); return; }
        if (player.Weapon == null) { Debug.LogError("[Enemy::SetupGetHit] player weapon is null"); return; }

        Attack attack = player.Weapon.GetComponent<Attack>();
        if (attack == null) { Debug.LogError("[Enemy::SetupGetHit] player attack is null"); return; }

        attack.IsHit
              .ObserveEveryValueChanged(x => x.Value)
              .Subscribe(x =>
              {
                    Attack attackInfo = player.Weapon.GetComponent<Attack>();
                    if (x && attackInfo.Name == this.name)
                    {                      
                        Status.HP.Value -= attackInfo.Skill.Damage.Value;
                        _animationMng.ChangeAnimation(Status.HP.Value <= 0
                            ? AnimationType.Death : AnimationType.GetHit);
                        attack.IsHit.Value = false;
                    }
              }).AddTo(gameObject);
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (_target == null)
        {
            Debug.LogError("[Enemy::Update] Target is null");
            return;
        }
        if (_agent == null)
        {
            Debug.LogError("[Enemy::Update] Agent is null");
            return;
        }

        if (_agent.pathStatus != NavMeshPathStatus.PathInvalid)
        {
            _agent.destination = _target.transform.position;

        }
    }
}
