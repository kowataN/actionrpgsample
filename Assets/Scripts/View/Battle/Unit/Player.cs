﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]
public class Player : UnitCore, IStatusCalc
{    
    private Rigidbody _rb;
    public GameObject Weapon;
    private Attack _attack;
    public float MoveSpeed = 0.1f;

    protected void Awake()
    {
        InitAnimator();
        InitRigidBody();
        InitCharacterController();
        InitWeapon();

        #region 移動関連

        // 移動
        InputManager.Instance.AxisKey
                    .Where(_ => InputManager.Instance.IsMove())
                    .Where(_ =>
                        !_animationMng.IsAnimation(AnimationType.Attack1) &&
                        !_animationMng.IsAnimation(AnimationType.Death)
                    )
                    .Subscribe(x =>
                    {
                        if (!_animationMng.IsAnimation(AnimationType.Run))
                        {
                            _animationMng.ChangeAnimation(AnimationType.Run);
                        }
                        // 移動
                        Vector3 value = x * MoveSpeed;

                        // 回転
                        UpdateRotation(value);

                        // 重力
                        value.y -= 9.8f * Time.deltaTime;
                        _charaCtrl.Move(value);

                    })
                    .AddTo(gameObject);

        // 待機
        InputManager.Instance.AxisKey
                    .Where(_ => !InputManager.Instance.IsMove())
                    .Where(_ => _animationMng.IsAnimation(AnimationType.Run))
                    .Subscribe(x =>
                    {
                        if (!_animationMng.IsAnimation(AnimationType.Idle))
                        {
                            _animationMng.ChangeAnimation(AnimationType.Idle);
                        }
                    })
                    .AddTo(gameObject);
        #endregion

        this.UpdateAsObservable()
            .Where(_ => InputManager.Instance.GetKeyDown(KeyCode.Space))
            .Where(_ =>
                _animationMng.IsAnimation(AnimationType.Idle) ||
                _animationMng.IsAnimation(AnimationType.Run)
            )
            .Subscribe(_ =>
            {
                _attack.SetNormalAttack();
                _animationMng.ChangeAnimation(AnimationType.Attack1);
            })
             .AddTo(gameObject);

        _ = Status.Level.DistinctUntilChanged()
            .Subscribe(_ =>
            {
                Status.HP.Value =
                    Status.MaxHP.Value = GetMaxHp(Status.Level.Value);
            })
            .AddTo(gameObject);
    }

    private void InitWeapon()
    {
        _attack = Weapon.GetComponent<Attack>();
        if (_attack == null)
        {
            Debug.LogError("[Player::InitWeapon] attack is null");
            return;
        }

        _attack.Init(10, "Enemy");
    }

    public void Init(int hp)
    {
        Status.Level = new IntReactiveProperty(1);
        Status.MaxHP = new IntReactiveProperty(hp);
        Status.HP = new IntReactiveProperty(hp);
        Status.NextExp = new IntReactiveProperty(10);
    }

    private void InitRigidBody()
    {
        _rb = GetComponent<Rigidbody>();
        if (_rb == null)
        {
            Debug.LogError("[Player::InitRigidBody] Rigidbody is null");
            return;
        }
        _rb.isKinematic = true;
        _rb.useGravity = true;
    }

    protected override void InitCharacterController()
    {
        _charaCtrl = GetComponent<CharacterController>();
        if (_charaCtrl == null)
        {
            Debug.LogError("[Player::InitCharacterController] CharacterController is null");
            return;
        }
    }

    /// <summary>
    /// 向き調整など
    /// </summary>
    private void UpdateRotation(Vector3 vector)
    {
        // 回転
        Vector3 forward = Vector3.Slerp(transform.forward, vector,
            360 * Time.deltaTime / Vector3.Angle(transform.forward, vector));
        transform.LookAt(transform.position + forward);
    }

    public int GetMaxHp(int level)
    {
        return (level / 2) + 2;
    }

    public int GetNextExp(int level)
    {
        //return (level - 1) * 10;
        return 10;
    }

    public void AddExp(int exp)
    {
        Debug.Log("***** AddExp:" + exp.ToString());
        int addLevel = 0;
        bool loop = true;
        while (loop)
        {
            Debug.Log("*** NextExp:" + Status.NextExp.Value.ToString());
            if (Status.NextExp.Value <= exp)
            {
                // 残り必要経験値より獲得経験値が多い場合
                exp -= Status.NextExp.Value;
                // レベルアップ
                ++addLevel;
                Status.NextExp.Value = GetNextExp(Status.Level.Value + addLevel);
            }
            else
            {
                Status.NextExp.Value -= exp;
                loop = false;
            }
        }

        Status.Level.Value += addLevel;
        Status.MaxHP.Value = GetMaxHp(Status.Level.Value);
    }
}
