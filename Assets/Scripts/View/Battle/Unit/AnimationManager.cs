﻿using UnityEngine;
using UniRx;

/// <summary>
/// アニメーションタイプ（順番がそのまま優先順位）
/// </summary>
public enum AnimationType
{
    Idle = 0, Run, Attack1, Attack2, GetHit, Death,
};

public enum AnimationState
{
    Start = 0, Play, End,
};

public class AnimationManager : MonoBehaviour
{

    /// <summary>
    /// 現在のアニメーションタイプ
    /// </summary>
    /// <value>The crt animation.</value>
    public AnimationType CrtType { get; private set; } = AnimationType.Idle;
    public AnimationState CrtState { get; private set; } = AnimationState.Start;

    private Animator _Animator;

    public AnimeStateController AnimeStateController { get; private set; }

    public BoolReactiveProperty Dead = new BoolReactiveProperty(false);

    private const float StartNormalizedTime = 0.0f;

    private AnimationManager() { }
    public AnimationManager(Animator anim)
    {
        SetAnimator(anim);
    }

    private void Awake() { }

    private void SetAnimator(Animator anim)
    {
        _Animator = anim;
        if (_Animator == null)
        {
            return;
        }

        AnimeStateController = _Animator.GetBehaviour<AnimeStateController>();
        if (AnimeStateController == null)
        {
            Debug.LogError("[AnimationManager::SetAnimator] AnimeStateController is null");
            return;
        }

        // 攻撃終了時の処理
        AnimeStateController.AttackState
            .Where(x => x == AnimeState.End)
            .Subscribe(_ => ChangeAnimation(AnimationType.Idle));

        // 攻撃受け終了時の処理
        AnimeStateController.GetHitState
            .Where(x => x == AnimeState.End)
            .Subscribe(_ => ChangeAnimation(AnimationType.Idle));

        // 死亡モーション
        AnimeStateController.DeadState
            .Where(x => x == AnimeState.End)
            .Subscribe(_ =>
            {
                Debug.Log("AnimationManager Dead End");
                Dead.Value = true;
                _Animator.speed = 0;
            });
    }

    /// <summary>
    /// アニメーションを変更します
    /// </summary>
    /// <param name="type">AnimationType</param>
    public void ChangeAnimation(AnimationType type)
    {
        // アニメーション切り替え
        _Animator.SetInteger("AnimationNo", (int)type);

        CrtType = type;
    }

    /// <summary>
    /// 現在が指定アニメーションかを返します
    /// </summary>
    /// <returns><c>true</c>, 指定アニメーション, <c>false</c> 別のアニメーション.</returns>
    /// <param name="type">AnimationType</param>
    public bool IsAnimation(AnimationType type)
    {
        return CrtType == type;
    }
}
