﻿public interface IStatusCalc
{
    int GetMaxHp(int level);
    int GetNextExp(int level);
}
