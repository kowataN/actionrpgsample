﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class AreaController : MonoBehaviour
{
    public BoolReactiveProperty InRange = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsStay = new BoolReactiveProperty(false);
    public SphereCollider _Area;
    [SerializeField] private string _TargetTag;

    private void Awake()
    {
        _Area = GetComponent<SphereCollider>();
        if (_Area == null)
        {
            Debug.LogError("[SearchArea::Awake] SphereCollider is null");
            return;
        }

        // エリアに侵入した際の処理
        _Area.OnTriggerEnterAsObservable()
             .Where(x => x.gameObject.tag == _TargetTag)
             .Subscribe(_ =>
             {
                 InRange.Value = true;
             }).AddTo(gameObject);

        // エリアに侵入している
        _Area.OnTriggerStayAsObservable()
             .Where(x => x.gameObject.tag == _TargetTag)
             .Subscribe(_ =>
             {
                 IsStay.Value = false;
             }).AddTo(gameObject);

        // エリア内から外に出た時の処理
        _Area.OnTriggerExitAsObservable()
             .Where(x => x.gameObject.tag == _TargetTag)
             .Subscribe(_ =>
             {
                 InRange.Value = false;
             }).AddTo(gameObject);
    }

    public void SetRadius(int redius)
    {
        _Area.radius = redius;
    }
}
