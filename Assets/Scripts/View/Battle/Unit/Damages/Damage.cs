﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Damage
{
    /// <summary>
    /// 攻撃者
    /// </summary>
    public IAttacker Attacker;

    /// <summary>
    /// ダメージ値
    /// </summary>
    public int Value;
}