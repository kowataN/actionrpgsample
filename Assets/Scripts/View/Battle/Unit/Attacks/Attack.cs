﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Attack : MonoBehaviour
{
    [SerializeField] private Damage _damage;
    [SerializeField] private string _targetTag;

    // 誰にあったのかを一旦名前でとる
    private string _name;
    public string Name => _name;


    /// <summary>
    /// 武器と当たっているかどうかを監視
    /// </summary>
    public BoolReactiveProperty IsHit = new BoolReactiveProperty(false);
    public BaseSkillCalc Skill { get; private set; }

    public void Init(int power, string targetTag)
    {
        _damage.Value = power;
        _targetTag = targetTag;

        this.OnTriggerEnterAsObservable()
            .Where(col => col.CompareTag(_targetTag))
            .Subscribe(col =>
            {
                IsHit.Value = true;
                _name = col.name;
            });
    }

    public void SetNormalAttack()
    {
        NormalAttack normalAttack = new NormalAttack
        {
            Damage = _damage,
            Name = _name
        };

        Skill = normalAttack;
    }
}
