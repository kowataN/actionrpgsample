﻿using UnityEngine;

public class ProcessAttack : MonoBehaviour
{
    public Collider _Collider;

    // Start is called before the first frame update
    private void AttackStart()
    {
        _Collider.enabled = true;
    }

    // Update is called once per frame
    private void AttackEnd()
    {
        _Collider.enabled = false;
    }
}
