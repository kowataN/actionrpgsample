﻿using UnityEngine;

/// <summary>
/// スキル計算基底クラス
/// </summary>
public abstract class BaseSkillCalc
{
    /// <summary>
    /// ダメージ情報
    /// </summary>
    public Damage Damage;

    public string Name;

    public BaseSkillCalc()
    {
    }
}
