﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject _Target;
    private Vector3 _Offset;

    // Use this for initialization
    private void Start() { }

    public void Init(GameObject target)
    {
        _Target = target;
        _Offset = transform.position - _Target.transform.position;

        this.FixedUpdateAsObservable()
            .Subscribe(_ =>
            {
                transform.position = _Target.transform.position + _Offset;
            }).AddTo(gameObject);
    }
}
