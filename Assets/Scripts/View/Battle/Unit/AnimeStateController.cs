﻿using UnityEngine;
using UniRx;

public enum AnimeState
{
    Init, Loop, End
}

public class AnimeStateController : StateMachineBehaviour
{
    [SerializeField] private AnimationClip clipIdle;
    [SerializeField] private AnimationClip clipRun;
    [SerializeField] private AnimationClip clipAttack1;
    [SerializeField] private AnimationClip clipAttack2;
    [SerializeField] private AnimationClip clipGetHit;
    [SerializeField] private AnimationClip clipDead;

    public ReactiveProperty<AnimeState> AttackState = new ReactiveProperty<AnimeState>(AnimeState.Init);
    public ReactiveProperty<AnimeState> GetHitState = new ReactiveProperty<AnimeState>(AnimeState.Init);
    public ReactiveProperty<AnimeState> DeadState = new ReactiveProperty<AnimeState>(AnimeState.Init);

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("OnStateEnter : " + GetStateName(stateInfo));
        if (stateInfo.IsName(clipAttack1.name) ||
            stateInfo.IsName(clipAttack2.name))
        {
            AttackState.Value = AnimeState.Loop;
        }

        if (stateInfo.IsName(clipGetHit.name))
        {
            GetHitState.Value = AnimeState.Loop;
        }

        if (stateInfo.IsName(clipDead.name))
        {
            DeadState.Value = AnimeState.Loop;
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("OnStateExit : " + GetStateName(stateInfo));
        if (stateInfo.IsName(clipAttack1.name) ||
            stateInfo.IsName(clipAttack2.name))
        {
            AttackState.Value = AnimeState.End;
            Debug.Log("attack1 end");
        }

        if (stateInfo.IsName(clipGetHit.name))
        {
            GetHitState.Value = AnimeState.End;
        }

        if (stateInfo.IsName(clipDead.name))
        {
            DeadState.Value = AnimeState.End;
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName(clipAttack1.name) ||
            stateInfo.IsName(clipAttack2.name))
        {
            // 攻撃モーションは一回再生したら終了させる
            AttackState.Value = stateInfo.normalizedTime >= 1.0f ? AnimeState.End : AnimeState.Loop;
            if (AttackState.Value == AnimeState.End)
            {
                //Debug.Log("OnStateUpdate END : " + GetStateName(stateInfo));
            }
        }

        if (stateInfo.IsName(clipGetHit.name))
        {
            // ダメージモーションは一回再生したら終了させる
            GetHitState.Value = stateInfo.normalizedTime >= 1.0f ? AnimeState.End : AnimeState.Loop;
            if (GetHitState.Value == AnimeState.End)
            {
                //Debug.Log("OnStateUpdate END : " + GetStateName(stateInfo));
            }
        }

        if (stateInfo.IsName(clipDead.name))
        {
            // 死亡モーションは一回再生したら終了させる
            DeadState.Value = stateInfo.normalizedTime >= 1.0f ? AnimeState.End : AnimeState.Loop;
            if (DeadState.Value == AnimeState.End)
            {
                //Debug.Log("OnStateUpdate END : " + GetStateName(stateInfo));
            }
        }
    }

    #region デバッグ用
    private string GetStateName(AnimatorStateInfo stateInfo)
    {
        if (stateInfo.IsName(clipIdle.name))
        {
            return clipIdle.name;
        }
        if (stateInfo.IsName(clipRun.name))
        {
            return clipRun.name;
        }
        if (stateInfo.IsName(clipAttack1.name))
        {
            return clipAttack1.name;
        }
        if (stateInfo.IsName(clipAttack2.name))
        {
            return clipAttack2.name;
        }
        if (stateInfo.IsName(clipGetHit.name))
        {
            return clipGetHit.name;
        }
        if (stateInfo.IsName(clipDead.name))
        {
            return clipDead.name;
        }
        return "None";
    }
    #endregion
}
