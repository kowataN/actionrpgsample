﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugBattle : MonoBehaviour
{
    [SerializeField] private Button _titleButton;

    // Start is called before the first frame update
    void Start()
    {
        // タイトルボタン押下処理
        _titleButton.OnClickAsObservable()
            .Subscribe(_ =>
            {
                SceneManager.LoadScene("Title", LoadSceneMode.Single);
            }).AddTo(gameObject);
    }
}
